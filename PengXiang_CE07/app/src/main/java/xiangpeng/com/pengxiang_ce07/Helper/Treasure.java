package xiangpeng.com.pengxiang_ce07.Helper;

import android.graphics.Point;
import java.io.Serializable;

public class Treasure implements Serializable {
    private final String item;
    private final String value;
    private final Point point;

    public Treasure(String item, String value, Point point) {
        this.item = item;
        this.value = value;
        this.point = point;
    }

    public String getItem() {
        return item;
    }

    public String getValue() {
        return value;
    }

    public Point getPoint() {
        return point;
    }

}
