package xiangpeng.com.pengxiang_ce07.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;

import xiangpeng.com.pengxiang_ce07.Fragment.GameBoardFragment;
import xiangpeng.com.pengxiang_ce07.GameSurface;
import xiangpeng.com.pengxiang_ce07.Helper.FoundTreasure;
import xiangpeng.com.pengxiang_ce07.R;

public class GameBoardActivity extends AppCompatActivity {
    public static final String FOUND_TREASURE = "xiangpeng.com.pengxiang_ce07.GameBoardActivity.FOUND_TREASURE";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_board);
        // show fragment
        getFragmentManager().beginTransaction()
                .replace(
                        R.id.activity_game_board_container,
                        GameBoardFragment.newInstance(),
                        GameBoardFragment.TAG
                ).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.game_board_action_inventory, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ArrayList<FoundTreasure> found_treasure = GameSurface.getFoundTreasures();
        Intent inventory_intent = new Intent();
        inventory_intent.setClass(this, InventoryActivity.class);
        // pass it to the inventory
        inventory_intent.putExtra(FOUND_TREASURE, found_treasure);
        startActivity(inventory_intent);

        return super.onOptionsItemSelected(item);
    }
}
