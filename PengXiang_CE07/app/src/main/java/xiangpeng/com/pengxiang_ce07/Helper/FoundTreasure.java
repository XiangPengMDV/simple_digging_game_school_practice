package xiangpeng.com.pengxiang_ce07.Helper;

import java.io.Serializable;

public class FoundTreasure implements Serializable {
    private final String item_name;
    private final String item_value;

    public FoundTreasure(String item_name, String item_value) {
        this.item_name = item_name;
        this.item_value = item_value;
    }

    public String getItem_name() {
        return item_name;
    }

    public String getItem_value() {
        return item_value;
    }
}
