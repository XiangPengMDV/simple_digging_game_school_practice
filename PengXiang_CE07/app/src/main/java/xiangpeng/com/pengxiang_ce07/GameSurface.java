package xiangpeng.com.pengxiang_ce07;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;
import java.util.ArrayList;
import xiangpeng.com.pengxiang_ce07.Helper.FileUtils;
import xiangpeng.com.pengxiang_ce07.Helper.FoundTreasure;
import xiangpeng.com.pengxiang_ce07.Helper.Treasure;

public class GameSurface extends SurfaceView implements SurfaceHolder.Callback  {
    //////////////////
    // Constructors //
    //////////////////
    public GameSurface(Context context) {
        super(context);
    }

    public GameSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameSurface(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GameSurface(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    //////////////////
    // Constructors //
    //////////////////

    private Rect mDimensions; // drawing dimension
    private RectF mBounds; // bounds for text on top

    private Bitmap mBackground; // grass background
    private Paint mBlankPoint; // blank paint to hold that bitmap
    private Paint mTextPaint; // just a text to show on top center

    private Bitmap mDirtHolePaint; // dirt hole
    private Paint mHoleBlankPoint; // container paint
    private ArrayList<Point> mPoints; // dirt hole position

    private ArrayList<Treasure> mTreasures; // list of treasure objects
    private Rect mTreasureRangeRect; // canvas size to alloc treasure position
    private static ArrayList<FoundTreasure> foundTreasures; // stuff we found

    private boolean isGameInit = false;

    ///////////////
    // Callbacks //
    ///////////////
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        storeDimensions(surfaceHolder);

        // this gets called every time, cant init game here
        // also need to get dimensions before set treasures
        if (mTreasureRangeRect == null && !isGameInit) {
            // if nothing, just assign the size
            mTreasureRangeRect = mDimensions;

            mTreasures = FileUtils.setTreasuresWithPosition(getContext(), mTreasureRangeRect);

            isGameInit = true;
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        setWillNotDraw(false);
        getHolder().addCallback(this);

        // set up background here
        Resources res = getResources();
        mBackground = BitmapFactory.decodeResource(res, R.drawable.field);
        mBlankPoint = new Paint();

        // set up dirt hole and position
        mDirtHolePaint = BitmapFactory.decodeResource(res, R.drawable.hole);
        mHoleBlankPoint = new Paint();
        mPoints = new ArrayList<>();

        // set game text here
        mTextPaint = new Paint();
        mTextPaint.setColor(Color.RED);
        mTextPaint.setTextSize(60.0f);

        foundTreasures = new ArrayList<>();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the background image to fill the entire dimensions of the canvas
        canvas.drawBitmap(mBackground, null, mDimensions, mBlankPoint);

        // text and text position
        String game_text = "TREASURE LEFT: " + mTreasures.size();
        mBounds.right = mTextPaint.measureText(game_text, 0, game_text.length());
        mBounds.left = (mDimensions.width() - mBounds.right) / 2;

        // Draw a dirt hole at each touch point
        for (Point p : mPoints) {
            canvas.drawBitmap(
                    mDirtHolePaint,                       // paint
                    p.x - mDirtHolePaint.getWidth() / 2,  // x position
                    p.y - mDirtHolePaint.getHeight() / 2, // y position
                    mHoleBlankPoint                       // container
            );
        }

        // Draw some text in the middle of the screen
        // draw this last so it will always show over everything
        canvas.drawText(
                game_text,    // text to show
                mBounds.left, // start point
                60,           // size
                mTextPaint);  // paint object
    } // _end onDraw()

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Check if this touch event is a down event
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // Add the point to the list
            mPoints.add(new Point(
                    (int) event.getX(),
                    (int) event.getY()
            ));

            int start_x = (int) event.getX() - mDirtHolePaint.getWidth() / 2; // start point x
            int start_y = (int) event.getY() - mDirtHolePaint.getHeight() / 2; // start point y
            checkTreasureOnTap(start_x, start_y, mDirtHolePaint.getWidth(), mDirtHolePaint.getHeight());

            // Tell the view to redraw on the next frame
            postInvalidate();
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        storeDimensions(surfaceHolder);
    }

    private void storeDimensions(SurfaceHolder _holder) {
        // Lock the canvas to get an instance of it back.
        Canvas canvas = _holder.lockCanvas();

        // Retrieve the dimensions and hold onto them for later.
        mDimensions = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());
        mBounds = new RectF(mDimensions);

        // Release the canvas and post a draw.
        _holder.unlockCanvasAndPost(canvas);
    }

    private void checkTreasureOnTap(int _start_point_x, int _start_point_y, int _end_point_x, int _end_point_y) {
        // to scan a small area and check points
        int end_x = _start_point_x + _end_point_x;
        int end_y = _start_point_y + _end_point_y;

        for (int x = _start_point_x; x < end_x; x++) {
            for (int y = _start_point_y; y < end_y; y++) {
                // loop through treasures list to check position
                for (int i = 0; i < mTreasures.size(); i++) {
                    // if found treasure
                    if (x == mTreasures.get(i).getPoint().x && y == mTreasures.get(i).getPoint().y) {
                        // show player found something
                        // Found: Item! (value: 500)
                        String treasure_text = "Found: " + mTreasures.get(i).getItem() + "! (value: " + mTreasures.get(i).getValue() + ")";
                        Toast.makeText(getContext(), treasure_text, Toast.LENGTH_SHORT).show();
                        // add to the found treasure
                        // remove from the treasure
                        foundTreasures.add(new FoundTreasure(mTreasures.get(i).getItem(), mTreasures.get(i).getValue()));
                        mTreasures.remove(i);
                        break;
                    }
                }
            }
        }
    } // _end checkTreasureOnTap()

    public static ArrayList<FoundTreasure> getFoundTreasures() {
        return foundTreasures;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {}
    ///////////////
    // Callbacks //
    ///////////////
}
