package xiangpeng.com.pengxiang_ce07.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xiangpeng.com.pengxiang_ce07.R;

public class GameBoardFragment extends Fragment {
    public static final String TAG = "xiangpeng.com.pengxiang_ce07.Fragment.GameBoardFragment";

    public static GameBoardFragment newInstance() {
        Bundle args = new Bundle();
        GameBoardFragment fragment = new GameBoardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gamen_board, container, false);
    }
}
