package xiangpeng.com.pengxiang_ce07.Fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import xiangpeng.com.pengxiang_ce07.Activity.GameBoardActivity;
import xiangpeng.com.pengxiang_ce07.R;


public class MenuFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = "xiangpeng.com.pengxiang_ce07.Fragment.MenuFragment";

    public static MenuFragment newInstance() {
        Bundle args = new Bundle();
        MenuFragment fragment = new MenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Button btn_start;
    private Button btn_credit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_start, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btn_start = (Button) getActivity().findViewById(R.id.fragment_start_start_digging_button);
        btn_credit = (Button) getActivity().findViewById(R.id.fragment_start_credit_button);

        btn_start.setOnClickListener(this);
        btn_credit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // click start
        if (v.getId() == btn_start.getId()) {
            // start game board activity
            Intent start_intent = new Intent();
            start_intent.setClass(getActivity(), GameBoardActivity.class);
            startActivity(start_intent);

        }
        // click credit
        else if (v.getId() == btn_credit.getId()) {
            // switch to credit fragment to show credit
            getFragmentManager().beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .replace(
                            R.id.activity_start_container,
                            CreditFragment.newInstance(),
                            CreditFragment.TAG
                    ).commit();
        }

    }

}
