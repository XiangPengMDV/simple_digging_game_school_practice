package xiangpeng.com.pengxiang_ce07.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import xiangpeng.com.pengxiang_ce07.Fragment.MenuFragment;
import xiangpeng.com.pengxiang_ce07.R;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        // show fragment
        getFragmentManager().beginTransaction()
                .replace(
                        R.id.activity_start_container,
                        MenuFragment.newInstance(),
                        MenuFragment.TAG
                ).commit();
    }
}
