package xiangpeng.com.pengxiang_ce07.Helper;


import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import xiangpeng.com.pengxiang_ce07.R;

public class FileUtils {

    @Nullable
    public static ArrayList<Treasure> setTreasuresWithPosition(Context context, Rect treasure_size_rect) {
        ArrayList<Treasure> treasures;
        int x_bound = treasure_size_rect.width();
        int y_bound = treasure_size_rect.height();

        InputStream is = context.getResources().openRawResource(R.raw.items);
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr, 8192);

        treasures = new ArrayList<>();

        try {
            String test;
            while ((test = br.readLine()) != null) {
                // readLine() returns null if no more lines in the file
                String[] str = test.split(",");
                int p_x = 0;
                int p_y = 0;

                // first of all, I want to check if my treasure has anything in it
                if (treasures.size() <= 0) {
                    // if not, add its first item to it
                    p_x = makeRandInt(x_bound);
                    p_y = makeRandInt(y_bound);
                    treasures.add(new Treasure(
                            str[0],
                            str[1],
                            new Point(
                                    p_x, p_y
                            )
                    ));
                } else {
                    // if I have something in treasure list, I want to make sure that positions are unique
                    // check it by looping through the treasures list
                    for (int i = 0; i < treasures.size(); i++) {
                        p_x = makeRandInt(x_bound);
                        p_y = makeRandInt(y_bound);

                        while ((p_x == treasures.get(i).getPoint().x && p_y == treasures.get(i).getPoint().y)
                                || (p_x == 0 || p_y == 0)) {
                            // if same position, keep random until we got a different result
                            // also skip number 0
                            p_x = makeRandInt(x_bound);
                            p_y = makeRandInt(y_bound);
                        }

                    } // _ for (int i = 0; i < treasures.size(); i++)

                    // then add it to the treasures
                    treasures.add(new Treasure(
                            str[0],
                            str[1],
                            new Point(
                                    p_x, p_y
                            )
                    ));
                } // _end else block | if (treasures.size() <= 0)
            } // _ while ((test = br.readLine()) != null)
            isr.close();
            is.close();
            br.close();
            // return our final result
            return treasures;
        } catch (IOException e) {
            e.printStackTrace();
            // just return null
            return null;
        }
    } // _end setTreasuresWithPosition()

    private static int makeRandInt(int high_limit) {
        Random random = new Random();
        return random.nextInt(high_limit);
    }
}
