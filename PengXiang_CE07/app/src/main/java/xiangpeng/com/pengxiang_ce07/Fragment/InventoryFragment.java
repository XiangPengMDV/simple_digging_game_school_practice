package xiangpeng.com.pengxiang_ce07.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import xiangpeng.com.pengxiang_ce07.Activity.GameBoardActivity;
import xiangpeng.com.pengxiang_ce07.Helper.FoundTreasure;
import xiangpeng.com.pengxiang_ce07.R;

public class InventoryFragment extends Fragment {
    public static final String TAG = "xiangpeng.com.pengxiang_ce07.Fragment.InventoryFragment";

    public static InventoryFragment newInstance() {
        Bundle args = new Bundle();
        InventoryFragment fragment = new InventoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_inventory, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        int mTotalValue = 0;

        ListView list_view = (ListView) getActivity().findViewById(R.id.fragment_inventory_list_view);
        TextView total_value = (TextView) getActivity().findViewById(R.id.fragment_inventory_total_value_textView);

        Intent intent = getActivity().getIntent();
        ArrayList<FoundTreasure> found_treasure;
        ArrayList<String> inventory = new ArrayList<>();

        //noinspection unchecked
        found_treasure = (ArrayList<FoundTreasure>) intent.getSerializableExtra(GameBoardActivity.FOUND_TREASURE);

        // so we check our found treasure to get type and value
        if (found_treasure != null) {
            // so we have found treasure here, lets try to manage it
            for (int i = 0; i < found_treasure.size(); i++) {
                String treasure_type = found_treasure.get(i).getItem_name();
                String treasure_value_string = found_treasure.get(i).getItem_value();

                String display_text = "[" + treasure_type + "]  |  $$: " + treasure_value_string;
                inventory.add(display_text);

                // calculate the total value
                mTotalValue += Integer.parseInt(treasure_value_string);
            }
            String total_value_text = "Total Value : " + String.valueOf(mTotalValue);
            total_value.setText(total_value_text);
        } else {
            String empty_text = "Total Value : 0";
            total_value.setText(empty_text);
        }

        ArrayAdapter<String> inventory_adapter;
        inventory_adapter = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                inventory
        );

        list_view.setAdapter(inventory_adapter);

    } // _end onActivityCreated()
}

