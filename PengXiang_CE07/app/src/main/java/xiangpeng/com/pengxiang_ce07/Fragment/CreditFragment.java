package xiangpeng.com.pengxiang_ce07.Fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import xiangpeng.com.pengxiang_ce07.R;

public class CreditFragment extends Fragment {
    public static final String TAG = "xiangpeng.com.pengxiang_ce07.Fragment.CreditFragment";

    public static CreditFragment newInstance() {
        Bundle args = new Bundle();
        CreditFragment fragment = new CreditFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_credit, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Button btn_back = (Button) getActivity().findViewById(R.id.fragment_credit_back_button);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .replace(
                                R.id.activity_start_container,
                                MenuFragment.newInstance(),
                                MenuFragment.TAG
                        ).commit();
            }
        });
    }
}
