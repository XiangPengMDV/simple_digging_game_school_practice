package xiangpeng.com.pengxiang_ce07.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import xiangpeng.com.pengxiang_ce07.Fragment.InventoryFragment;
import xiangpeng.com.pengxiang_ce07.R;

public class InventoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
        // show fragment
        getFragmentManager().beginTransaction()
                .replace(
                        R.id.activity_inventory_container,
                        InventoryFragment.newInstance(),
                        InventoryFragment.TAG
                ).commit();
    }
}
